#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <set>
#include <deque>
#include <thread>

const unsigned int max_threads = 8;

using namespace std;

bool offensive(const string& secret, const string& swear) {
    if (swear.length() > secret.length()) {
        return false;
    }

    if (swear.length() == secret.length()) {
        return (swear == secret);
    }

    auto itr = secret.begin();
    for (auto sw_itr = swear.cbegin(); sw_itr != swear.cend();  sw_itr++) {
        const char sw_char = *sw_itr;

        auto next_letter = find(secret.begin(), secret.end(), sw_char);

        for (; next_letter != secret.end(); next_letter++) {
            if (next_letter == itr) {
                return false;
            }
        }
       
        itr = next_letter;

        if (itr  == secret.end()) {
            return false;
        }
    }

    return true;
}

unsigned int problem_count(const set<string>& dict, const string& swear) {
    unsigned int pc = 0;
    for (auto word : dict) {
        if (offensive(word, swear)) {
            pc++;
        }
    }
    return pc;
}

int regular_challenge(istream& wds) {
    string secret, swear;

    wds >> secret;
    wds >> swear;

    transform(secret.begin(), secret.end(), secret.begin(), ::toupper);
    transform(swear.begin(), swear.end(), swear.begin(), ::toupper);

    cout << boolalpha << offensive(secret, swear) << endl;

    return 0;
}

int optional1(istream& args) { 
    string swear;
    args >> swear;

    set<string> dictionary;
    ifstream ifile("../enable1.txt");
    while (ifile.good()) {
        string wd;
        ifile >> wd;
        dictionary.insert(dictionary.end(), wd);
    }

    cout << "problem count for " << swear << ": " << problem_count(dictionary, swear) << endl;

    return 0;
}

int optional2(istream& args) {
    cout << "loading dictionary..." << endl;
    set<string> dictionary;
    ifstream ifile("../enable1.txt");
    while (ifile.good()) {
        string wd;
        ifile >> wd;
        transform(wd.begin(), wd.end(), wd.begin(), ::toupper);
        dictionary.insert(dictionary.end(), wd);
    }
    cout << "dictionary loaded" << endl;

    deque<pair<string, unsigned int>> answers;

    auto comp = [](pair<string, unsigned int> a, pair<string, unsigned int> b) {
        return (a.second) > (b.second);
    };

    unsigned long long done = 0;
    const unsigned long long todo = 26*26*26*26*26;

    auto tstart = chrono::system_clock::now();

    cout << endl;
    string c{"aaaaa"};
    for (c[0] = 'A'; c[0] <= 'Z'; c[0]++) {
        for (c[1] = 'A'; c[1] <= 'Z'; c[1]++) {
            for (c[2] = 'A'; c[2] <= 'Z'; c[2]++) {
                for (c[3] = 'A'; c[3] <= 'Z'; c[3]++) {
                    for (c[4] = 'A'; c[4] <= 'Z'; c[4]++) {
                        answers.push_back(make_pair(c, problem_count(dictionary, c)));
                        done++;
                    }
                }
                auto tp = chrono::system_clock::now();

                auto delay = chrono::duration_cast<std::chrono::seconds>(tp - tstart);

                double complete = static_cast<double>(done)/todo;
                
                cout << complete << endl;           

                sort(answers.begin(), answers.end(), comp);
                answers.erase(answers.begin() + 10, answers.end());
            }
        }
    }

    sort(answers.begin(), answers.end(), comp);
    answers.erase(answers.begin() + 10, answers.end());

    for (pair<string, unsigned int> a : answers) {
        cout << a.first << ": " << a.second << endl;
    }

    return 0;
}   

int main(int argc, char** argv) {
    if (argc == 1) {
        cout << "assuming regular challenge. enter two words separated by whitespace" << endl;
        return regular_challenge(cin);
    }

    string args;
    for (int i = 1; i < argc; i++) {
        args += string(argv[i]);
        if (i != (argc - 1)) {
            args += " ";
        }
    }

    istringstream argstream(args);
    
    string task;
    argstream >> task;

    if (task == string("-prob")) {
        return regular_challenge(argstream);
    }

    else if (task == string("-o1")) {
        return optional1(argstream);
    }

    else if (task == string("-o2")) {
        return optional2(argstream);
    }

    else {
        return regular_challenge(argstream);
    }

    return 0;
}
