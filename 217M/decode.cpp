#include <iostream>
#include <string>

using namespace std;

string omicron(const string& str) {
    string out_string = "";
    for (const char& c : str) {
        char new_c = c ^ (1 << 5);
        out_string += (new_c);
    }
    return out_string;
}

string hoth(const string& str) {
    string out_string = "";
    for (const char& c : str) {
        char new_c = c + 10;
        out_string += new_c;
    }
    return out_string;
}

string ryza(const string& str) {
    string out_string = "";
    for (const char& c : str) {
        char new_c = c - 1;
        out_string += new_c;
    }
    return out_string;
}

string htrae(const string& str) {
    string out_string = "";
    for (auto it = str.rbegin(); it != str.rend(); ++it) {
        out_string += *it;
    }
    return out_string;
}

int rate_solution(string sln) {
    int c = 0;
    for (char c : sln) {
        if (isalnum(c) || isspace(c)) {
            c++;
        }
    }
    return c;
}

int main(int argc, char** argv) {
    return 0;

}
