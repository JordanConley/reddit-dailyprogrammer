#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cctype>
#include <map>
#include <vector>
#include <set>

#include "dictionary.h"

using namespace std;
int main() {
    dictionary usdict("/usr/share/dict/american-english");

    map<char, char> hintmap;

    string challenge_string;
    getline(cin, challenge_string);
    for (size_t i = 0; i < challenge_string.length(); i++) {
        challenge_string[i] = toupper(challenge_string[i]);
    }

    int nHints;
    cin >> nHints;

    for (int i = 0; i < nHints; i++) {
        string hint;
        cin >> hint;

        if (hint.length() != 2) {
            cout << "INVALID HINT" << endl;
            return -1;
        }
        hintmap[toupper(hint[1])] =  toupper(hint[0]);
    }   

    // make the substitution from the hint
    string wild_card_subst(challenge_string.begin(), challenge_string.end());
    for (auto i = wild_card_subst.begin(); i != wild_card_subst.end(); i++) {
        auto mi = hintmap.find(*i);
        if (mi == hintmap.end()) {
            (*i) = '*';
        }
        else {
            (*i) = hintmap[*i];
        }
    }
    cout << challenge_string << endl;
    cout << wild_card_subst << endl;

    set<string> query_results;
    usdict.lookup(wild_card_subst, query_results);

    cout << "Query returned: " << endl;   
    for (string result: query_results) {
        cout << result << endl;
    }

    cout << "Checking solutions " << endl;
    vector<string> solutions;
    map<char, char> sln;
    for (string result: query_results) {
        cout << "- Checking " << result << endl;
        
        sln = hintmap;
        for (size_t i = 0, imax = min(result.size(), challenge_string.size()); i < imax; i++) {
            auto itr = sln.find(challenge_string[i]);

            cout << "-- i: " << i << "\t\tcs[i]: " << challenge_string[i] << "\tr[i]: " << result[i];// << endl;

            // never encountered this char yet
            if (itr == sln.end()) {
                cout << "\tb1" << endl;
                sln[challenge_string[i]] = toupper(result[i]);
            }

            else {
                // conflict
                cout << "\tb2";
                cout << "\ts[cs]: " << sln[challenge_string[i]] << endl;
                if (sln[challenge_string[i]] != toupper(result[i])) {
                    break;
                }       
            }

            if (i == (imax - 1)) {
                cout << result << " is valid. Key: ";
                for (auto p : sln) {
                    cout << p.first << ": " << p.second << ", " ;
                }
                cout << endl;
                solutions.push_back(result);
            }
        }
    }

    return 0;
}
