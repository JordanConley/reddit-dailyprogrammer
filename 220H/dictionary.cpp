#include <string>
#include <set>
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <sstream>
#include <vector>
#include "dictionary.h"

using namespace std;

void make_upper(string& str) {
    for (auto it = str.begin(); it != str.end(); it++) {
        *it = toupper(*it);
    }
}

dictionary::dictionary(const string& word_list) {
    load_from_file(word_list);
}

void dictionary::load_from_file(const string& path) {
    ifstream ifile(path);
    load_from_stream(ifile, true);
}

void dictionary::load_from_stream(istream& is, bool alpha) {
    while (is.good()) {
        string word;
        is >> word;

        make_upper(word);

        if (alpha) {
            words.insert(words.end(), word);      
        }
        else {
            words.insert(word);
        }
    }
}

bool dictionary::contains(const string& word) {
    return !(words.find(word) == words.end());
}


size_t dictionary::word_count() {
    return words.size();
}

void dictionary::print() {
    for (auto it : words) {
        cout << it << endl;
    }
}

bool matches(string key, const string& check, char wild_card = '*') {
    if (key.length() != check.length()) {
        return false;
    }

    make_upper(key);

    for (size_t i = 0; i < key.length(); i++) {
        if ((key[i] != wild_card) && (key[i] != check[i])) {
            return false;
        }
    }

    return true;
}

void dictionary::lookup(const string& key, set<string>& results) {
    if (key.find(' ') != key.npos) {
        return;
    }

    for (const string& word : words) {
        if (matches(key, word)) {
            results.insert(word);
        }
    }
}

void dictionary::lookup_multi(const string& keys, set<string>& results) {
    ostringstream keystream(keys);

    vector<set<string>> wordsets;

    while (keystream.good()) {
        string key;
        keystream >> key;

        map<string> word_results;
        lookup(key, word_results);

        wordsets.push_back(word_results);
    }

    if (wordsets.size() == 0) {
        return;
    }

    // flesh out the tree
    results.empty();

    auto tree_flesh[&](size_t level) -> void {
    };
}
