#pragma once
#include <iostream>
#include <string>
#include <set>

using namespace std;

class dictionary {
private:
    set<string> words;

public:
    dictionary(const string& wordlist);

    void load_from_file(const string& path);
    void load_from_stream(istream& is, bool sorted);

    bool contains(const std::string& word);

    size_t word_count();

    void print();
    
    void lookup(const string& key, set<string>& results);
    void lookup_multi(const string& key, set<string>& results);
};


