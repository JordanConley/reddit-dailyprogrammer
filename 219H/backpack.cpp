#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

struct solution {
    float weight;
    set<float> nuggets;
};

bool operator<(const solution &a, const solution &b) {
    return a.weight < b.weight;
}

solution best;
float capacity;

template <typename T>
void copy_without(const vector<T>& v, vector<T>& vout, T exc) {
    for (T t : v) {
        if (exc != t) {
            vout.push_back(t);
        }
    }
}

<template typename T>
void print_vector(const vector<T>& v) {
    cout << "{";
    for (auto it = v.cbegin(); it != --v.cend(); it++) {
        cout << *v << ", ";
    }
    if (v.size() != 0) {
        cout << *(--v.cend());
    }
    cout << "}";
}

void solve(solution stem, vector<float> remaining) {
    for (float nugget : remaining) {
        solution newsln = stem;
        if (newsln.weight + nugget <= capacity) {
            newsln.nuggets.insert(nugget);
            newsln.weight += nugget;
            if (best.weight < newsln.weight) {
                best = newsln;
            }

            print_vector<float>(remaining);
            cout << endl;
            vector<float> allminusnugget(remaining.size() - 1);
            copy_without(remaining, allminusnugget, nugget);
            print_vector<float>(allminusnugget);
            cout << endl;
            solve(newsln, allminusnugget);
        }
    }
}

int main(void) {
    cout << "reading cap" << endl;
    cin >> capacity;
    if (capacity <= 0) {
        cout << "bad capacity" << endl;
        return -1;
    }

    cout << "reading cnuggets" << endl;
    int cnuggets;
    cin >> cnuggets;
    if (cnuggets <= 0) {
        cout << "bad number of nuggets" << endl;
        return -2;
    }

    cout << "reading weights" << endl;
    vector<float> weights(cnuggets);
    for (int i = 0; i < cnuggets; i++) {
        cin >> weights[i];
    }

    solution stem;
    stem.weight = 0.f;
    cout << "solving..." << endl;

    solve(stem, weights);
    vector<float> floots{0.f, 0.2f, 1.4f, 1.35f, 1.4f, 16.f};
    vector<float> v2;
    copy_without(floots, v2, 0.2f);
    for (float v : v2) {
        cout << v << endl;
    }

    cout << best.weight << endl;
    for (float nugget : best.nuggets) {
        cout << nugget << endl;
    } 
}
