using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace negativebase
{
    class Program
    {
        static void Main(string[] args) {
            Console.Write("Base: ");
            String strBase = Console.ReadLine();
            Double nBase = Convert.ToDouble(strBase);

            Console.Write("Number in base {0}: ", nBase);
            String strNNumber = Console.ReadLine();
            char[] aOriginal = strNNumber.ToArray();

            Double result = 0;
            Int32 i = 0;

            foreach (char aChar in aOriginal.Reverse()) {
                int number = Convert.ToInt32(aChar - '0');
                result += number * Math.Pow(nBase, i);
                i++;
            }

            Console.WriteLine("Result: {0}", result);

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Press the any key to quit.");
            Console.ReadKey();
        }
    }
}
