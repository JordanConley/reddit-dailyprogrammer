import java.util.HashSet;
import java.util.Set;

/**
 * Created by jordan on 16/06/15.
 */
public class ToDoList {
    Set<String> items;

    public ToDoList() {
        items = new HashSet<>();
    }

    public void addItem(String item) {
        items.add(item);
    }

    public void deleteItem(String item) {
        items.remove(item);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        items.forEach(sb::append);
        return sb.toString();
    }

    public String viewList() {
        return toString();
    }
}
