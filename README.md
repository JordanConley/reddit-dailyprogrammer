# Daily Programmer
Some of my solutions to the challenges posted on /r/DailyProgrammer. Some problems are incomplete.

###223 Intermediate (C++)
Eel of Fortune. 

Completes basic challenge but not optionals.

###220 Hard (C++)
Substitution cypher decoder - WIP

###219 Hard (C++)
Backpack sorting algorithm - WIP

###218 Easy and Intermediate (Java)
Simple to-do list. Took all of about 2 minutes to write up.

###217 Intermediate (C++)
Alien language decoder - Mostly complete

###217 Easy (Java)
Lumberjack problem

###216 Easy (Java)
Texas Hold-Em setup. 216 Intermediate and Hard were designed to follow on after this one, but I never got around to attempting them.

###198 Intermediate (C#)
Base-Negative Numbers - Solution incorrect

###197 Easy (MATLAB)
ISBN Validator
