import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jordan on 26/05/15.
 */
public class Deck {
    private List<Card> cards;
    private Random rnd;

    public Deck() {
        cards = new ArrayList<>();
        rnd = new Random();

        reset();
    }

    public Card drawRandom() {
        int cardIndex = rnd.nextInt(cards.size());

        Card card = cards.get(cardIndex);
        cards.remove(cardIndex);
        return card;
    }

    public void reset() {
        for (int i = 1; i < 14; i++) {
            for (Card.Suits suit : Card.Suits.values()) {
                Card card = new Card(suit, i);

                cards.add(card);
            }
        }
    }
}
