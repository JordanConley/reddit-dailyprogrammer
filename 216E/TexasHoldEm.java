import java.util.Scanner;

/**
 * Created by jordan on 26/05/15.
 */
public class TexasHoldEm {
    public static void main(String[] args) {
        int numPlayers = getNumberOfPlayers();

        if (numPlayers < 2 || numPlayers > 8) {
            System.out.println("Bad number of players");
            return;
        }

        Game game = new Game(numPlayers);

        System.out.println(game);
    }

    private static int getNumberOfPlayers() {
        System.out.print("How many players? (2-8): ");

        Scanner inputScanner = new Scanner(System.in);
        int numPlayers = inputScanner.nextInt();
        inputScanner.close();

        return numPlayers;
    }
}
