import java.util.HashSet;
import java.util.Set;

/**
 * Created by jordan on 26/05/15.
 */
public class Player {
    private Set<Card> cards;
    private String name;

    public Player(String name) {
        cards = new HashSet<>();
        this.name = name;
    }

    public void giveCard(Card card) {
        if (cards.size() > 2) {
            System.out.println("Player " + name + " has too many cards!");
        }

        cards.add(card);
    }

    @Override
    public String toString() {
        return name + ": " + cards;
    }
}
