import java.util.ArrayList;
import java.util.List;

/**
 * Created by jordan on 26/05/15.
 */
public class Game {
    private Deck deck;
    private List<Player> players;

    private List<Card> flop;
    private Card turn;
    private Card river;

    public Game(int nPlayers) {
        deck = new Deck();
        players = new ArrayList<>();

        Player human = new Player("Human");
        human.giveCard(deck.drawRandom());
        human.giveCard(deck.drawRandom());
        players.add(human);

        nPlayers--;

        int i = 1;
        while (nPlayers >= players.size()) {
            Player ai = new Player("CPU " + i);
            ai.giveCard(deck.drawRandom());
            ai.giveCard(deck.drawRandom());
            players.add(ai);
            i++;
        }

        flop = new ArrayList<>();
        flop.add(deck.drawRandom());
        flop.add(deck.drawRandom());
        flop.add(deck.drawRandom());

        river = deck.drawRandom();

        turn = deck.drawRandom();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final String newLineChar = System.lineSeparator();

        for (Player player : players) {
            sb.append(player);
            sb.append(newLineChar);
        }

        sb.append(newLineChar);

        sb.append("Flop: ");
        sb.append(flop);
        sb.append(newLineChar);

        sb.append("Turn: ");
        sb.append(turn);
        sb.append(newLineChar);

        sb.append("River: ");
        sb.append(river);

        return sb.toString();
    }
}
