/**
 * Created by jordan on 26/05/15.
 */
public class Card {
    public enum Suits {Spades, Clubs, Hearts, Diamonds};

    private Suits suit;
    private int value;

    public Card(Suits suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        switch (value) {
            case 1:
                sb.append("Ace");
                break;

            case 11:
                sb.append("Jack");
                break;

            case 12:
                sb.append("Queen");
                break;

            case 13:
                sb.append("King");
                break;

            default:
                sb.append(value);
        }

        sb.append(" of ");
        sb.append(suit.name());

        return sb.toString();
    }
}
