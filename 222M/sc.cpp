#include <iostream>
#include <string>
#include <random>

using namespace std;

string encrypt(string msg, int key) {
    linear_congruential_engine<unsigned char, 41, 20, 215> rng(213);
    
    string encoded{""};
    for (char c : msg) {
        encoded += c ^ rng();
    }

    return encoded;
}

string decrypt(string msg, int key) {
    return encrypt(msg, key);
}

int main() {
    string msg = "bleh";
    cout << msg << endl;
    cout << encrypt(msg, 31252) << endl;
    cout << decrypt(encrypt(msg, 31252), 31252) << endl;
}
