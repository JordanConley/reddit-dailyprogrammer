#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#define COUNT_REGULAR_WORDS  4
const char* reg_words[COUNT_REGULAR_WORDS] = {
    "programmer",
    "ceramic",
    "onion",
    "alfalfa",
};

uint32_t garland(const char* word);
int compare(const char* a, const char* b);

void baseproblem();
void challenge1();
void challenge2();
void challenge3();

int main(int argc, char** argv) {
    printf("%s", "regular challenge:\n");
    baseproblem();

    printf("%s", "\nchallenge 2:\n");
    challenge2();

    return 0;
}

void baseproblem() {
    for (uint32_t i = 0; i < COUNT_REGULAR_WORDS; i++) {
        printf("%s-> %d\n", reg_words[i], garland(reg_words[i]));
    }
}

void challenge2() {
    FILE* enable1 = fopen("../enable1.txt", "r");
    if (enable1 == NULL) {
        printf("could not open dictionary for challenge 2\n");
        return;
    }

    char dict_buffer[32];
    char largest_garland[32];
    uint32_t largest_score = 0;

    while (fscanf(enable1, "%s", dict_buffer) != EOF) {
        uint32_t g = garland(dict_buffer);
        if (g > largest_score) {
            largest_score = g;
            strncpy(largest_garland, dict_buffer, 32);
        }
    }

    printf("largest: %s (%d)\n", largest_garland, largest_score);

    fclose(enable1);
}

uint32_t garland(const char* word) {
    const uint32_t len = strlen(word);
    if (len == 0) {
        return 0;
    }

    uint32_t largest = 0;

    for (int i = 0; i < len; i++) {
        if (compare(word, word + len - (i)) == 1) {
            largest = i > largest ? i : largest;
        } 
    }

    return largest;
}

int compare(const char* a, const char* b) {
    for (uint32_t i = 0; 1; i++) {
        if ((a[i] == 0) || (b[i] == 0)) {
            return 1;
        }
        if (a[i] != b[i]) {
            return 0;
        }
    }
    return -1;
}
