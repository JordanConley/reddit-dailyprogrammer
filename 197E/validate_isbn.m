function validate_isbn(isbn)

if (isa(isbn,'char')==0)
    fprintf('Invalid Input - Wrong data type\n');
    return;
end

isbn = regexprep(isbn, '-','');
if length(isbn) ~= 10
    fprintf('Invalid ISBN - Too few digits\n');
    return;
end

t = 0;
for i = 1:length(isbn) %this should be 10...
   t = t + (11 - t) * str2num(isbn(i));
end

if (mod(t,11) == 0)
    fprintf('Valid ISBN\n');
else
    fprintf('Invalid ISBN\n');
end

end