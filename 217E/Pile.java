/**
 * Created by jordan on 16/06/15.
 */
public class Pile {
    int size;
    int stacks[];
    int numToDistribute;

    public Pile(int numToDistribute, int size, int stacks[]) {
        this.stacks = stacks;
        this.size = size;
        this.numToDistribute = numToDistribute;
    }

    public int getSmallestStack() {
        int i = 0;

        for (int j = 0; j < (size*size); j++) {
            if (stacks[j] < stacks[i]) {
                i = j;
            }
        }

        return i;
    }

    public void solve() {
        while (numToDistribute > 0) {
            stacks[getSmallestStack()]++;
            numToDistribute--;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                sb.append(stacks[(size * i) + j]);
                sb.append(' ');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
