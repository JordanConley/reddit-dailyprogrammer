import java.util.Scanner;

/**
 * Created by jordan on 16/06/15.
 */
public class Lumberjack {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);

        int size = inputScanner.nextInt();
        inputScanner.nextLine();

        int remaining = inputScanner.nextInt();
        inputScanner.nextLine();

        int stack[] = new int[size * size];

        for (int i = 0; i < size; i++) {
            Scanner ls = new Scanner(inputScanner.nextLine());
            for (int j = 0; j < size; j++) {
                stack[(size * i) + j] = ls.nextInt();
            }
        }

        Pile pile = new Pile(remaining, size, stack);
        pile.solve();;
        System.out.println(pile);
    }
}
